
import * as tf from '@tensorflow/tfjs';
import '@tensorflow/tfjs-node';

export function coreConcepts() {
    // 2x3 Tensor
    const shape = [2, 3]; // 2 rows, 3 columns
    const aa = tf.tensor([1.0, 2.0, 3.0, 10.0, 20.0, 30.0], shape);
    aa.print(); // print Tensor values
    // Output: [[1 , 2 , 3 ],
    //          [10, 20, 30]]

    // The shape can also be inferred:
    const bb = tf.tensor([[1.0, 2.0, 3.0], [10.0, 20.0, 30.0]]);
    bb.print();
    // Output: [[1 , 2 , 3 ],
    //          [10, 20, 30]]

    const cc = tf.tensor2d([[1.0, 2.0, 3.0], [10.0, 20.0, 30.0]]);
    cc.print();
    // Output: [[1 , 2 , 3 ],
    //          [10, 20, 30]]

    // 3x5 Tensor with all values set to 0
    const zeros = tf.zeros([3, 5]);
    // Output: [[0, 0, 0, 0, 0],
    //          [0, 0, 0, 0, 0],
    //          [0, 0, 0, 0, 0]]
    zeros.print();

    const initialValues = tf.zeros([5]);
    const biases = tf.variable(initialValues); //initialize biases
    biases.print();

    const updateValues = tf.tensor1d([0, 1, 0, 1, 0]);
    biases.assign(updateValues);
    biases.print();

    const d = tf.tensor2d([[1.0, 2.0], [3.0, 4.0]]);
    const d_squared = d.square();
    d_squared.print();
    // Output: [[1, 4 ],
    //          [9, 16]]

    const e = tf.tensor2d([[1.0, 2.0], [3.0, 4.0]]);
    const f = tf.tensor2d([[5.0, 6.0], [7.0, 8.0]]);

    const e_plus_f = e.add(f);
    e_plus_f.print();
    // Output: [[6 , 8 ],
    //          [10, 12]]

    const sq_sum = e.add(f).square();
    sq_sum.print();
    // Output: [[36 , 64 ],
    //          [100, 144]]

    // All operations are also exposed as functions in the main namespace,
    // so you could also do the following:
    const sq_sum1 = tf.square(tf.add(e, f));
    sq_sum1.print();

    // Define function
    function predict(input: number) {
        // y = a * x ^ 2 + b * x + c
        // More on tf.tidy in the next section
        return tf.tidy(() => {
            const x = tf.scalar(input);

            const ax2 = a.mul(x.square());
            const bx = b.mul(x);
            const y = ax2.add(bx).add(c);

            return y;
        });
    }
    // Define constants: y = 2x^2 + 4x + 8
    const a = tf.scalar(2);
    const b = tf.scalar(4);
    const c = tf.scalar(8);

    // Predict output for input of 2
    const result = predict(2);
    result.print() // Output: 24

}