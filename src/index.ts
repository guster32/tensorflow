import { learnCoefficients } from './fittingCurve';
import { coreConcepts } from './coreConcepts';

coreConcepts();
learnCoefficients();