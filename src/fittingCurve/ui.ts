import { spawn } from 'child_process';
import { removeSync, openSync, writeSync, closeSync } from 'fs-extra';
import { tmpdir, EOL } from 'os';
import { resolve } from 'path';

const DAT_FILE = 'plot.dat';
const TMP_PATH = tmpdir();
const DAT_PATH = resolve(TMP_PATH, DAT_FILE);

// plot("set terminal qt; plot [-5:5] sin(x);")
export function plot(chart:string){
    spawn("gnuplot", ["-p", "-e", chart]);
};

export async function plotData(xs:any, ys:any, coeff:any) {
    removeSync(DAT_PATH);
    const datFileHandle = openSync(DAT_PATH, 'a');
    writeSync(datFileHandle, `#X Y${EOL}`);
    const xvalss = await xs.data();
    const yvalss = await ys.data();
    const xvals = await xs.data();
    const yvals = await ys.data();
    console.log(xvalss);
    console.log(yvalss);

    Array.from(yvals).forEach((y, i) => {
        writeSync(datFileHandle, `${xvals[i]} ${yvals[i]}${EOL}`);
    });
    closeSync(datFileHandle);
    const title = `Original Data with Coefficients: ${Object.keys(coeff).reduce((acc, key) => `${acc} ${key}:${coeff[key]}`, "")}`;
    let plotCommand = `set terminal qt;set loadpath '${TMP_PATH}'`
    plotCommand = `${plotCommand}; set title '${title}'`;
    plotCommand = `${plotCommand}; plot '${DAT_FILE}' using 1:2 with points`;
    plot(plotCommand);
}

export async function plotDataAndPredictions(xs:any, ys:any, preds:any) {
    removeSync(DAT_PATH);
    const datFileHandle = openSync(DAT_PATH, 'a');
    writeSync(datFileHandle, `#X Y Z1${EOL}`);
    const xvals = await xs.data();
    const yvals = await ys.data();
    const predVals = await preds.data();

    Array.from(yvals).forEach((y, i) => {
        writeSync(datFileHandle, `${xvals[i]} ${yvals[i]} ${predVals[i]}${EOL}`);
    });
    closeSync(datFileHandle);
    const title = 'Original Data with Predictions';
    let plotCommand = `set terminal qt;set loadpath '${TMP_PATH}'`
    plotCommand = `${plotCommand}; set title '${title}'`;
    plotCommand = `${plotCommand}; plot '${DAT_FILE}' using 1:2 with points, '${DAT_FILE}' using 1:3 with points`;
    plot(plotCommand);
}



